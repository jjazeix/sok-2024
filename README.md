# Season of KDE 2024 projects

## As a contributor
This repository is the place to propose your ideas to contribute to KDE during the Season of KDE program!

As contributor, create a new Issue with the template *sok_proposal*.
Your proposal will be created as *Confidential* meaning only you and the mentors can see it.
Potential intested will discuss with you in the comments section of your Issue.
Do not write any personal information in your proposal (no phone, address...) as they will become public once we reveal which projects will be part of this year SoK.

## As a mentor
Subscribe to the teams/season-of-kde team to be able to read all the proposals.
We have one meta issue, only visible to mentors and admins, to discuss internally about the proposals and select which one you are interested in.
Please discuss with contributors on their issues, and use only this issue to do the selection or if you need to discuss with admins about a proposal.
Do not update the labels in the issues manually!

## Timeline
Once the projects are selected, a label "accepted" or "rejected" will be added to the issues and the issues will become public.

At mid-term (if there is a mid-term), we will also add the labels "mid-accepted" or "mid-rejected".

At the end of the project, the labels "completed" / "not completed" will be added too.
