## Summary

(Which project do you want to do a proposal for)

## Proposal

(If possible, create an example project here on GitLab.com that exhibits the problematic
behavior, and link to it here in the bug report.
If you are using an older version of GitLab, this will also determine whether the bug has been fixed
in a more recent version)

## Timeline
(What/when you plan to do)

## How to reach you
(Element / IRC nickname / email).

Note: we don't need (yet) your personal address/phone, it will be necessary only if you succeed the SoK and wants to get goodies.

## Link to the document -> do we need this as we will directly comment in the proposal above? I would prefer we have a good template with which info should be given
link to the document where mentors can comment the proposal.

/label ~2024 ~proposal
/confidential
/cc @teams/season-of-kde
